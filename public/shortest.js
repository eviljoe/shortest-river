(function() {
    const CELL_LENGTH = 15;
    const CELL_BORDER_WIDTH = 1;
    const MAX_TICKS_BEFORE_PAINT = 64;
    const GRID_MAX_X = GRID[0].length;
    const GRID_MAX_Y = GRID.length;

    let start = {x: 0, y: 0};
    let end = {x: 0, y: 0};
    let distance = [];
    let previous = [];
    let shortestPath = new Set();
    let controlsEnabled = true;

    document.addEventListener('DOMContentLoaded', () => main());

    function main() {
        const canvas = document.getElementById('map');
        const ctx = canvas.getContext('2d');

        _addEventListeners();
        _reset();
        _setTimeToCalculateVisible(false);

        window.requestAnimationFrame(() => _draw(ctx));
    }

    function _addEventListeners() {
        const mapElem = document.querySelector('#map');
        mapElem.addEventListener('contextmenu', (e) => _mapClicked(e));
        mapElem.addEventListener('click', (e) => _mapClicked(e));

        document.querySelector('#calculate-button').addEventListener('click', () => _calculateShortestPath());
        document.querySelector('#reset-button').addEventListener('click', () => _reset());
    }

    function _draw(ctx) {
        _drawGrid(ctx);
        window.requestAnimationFrame(() => _draw(ctx));
    }

    function _drawGrid(ctx) {
        for(let y = 0; y < GRID_MAX_Y; y++) {
            for(let x = 0; x < GRID_MAX_X; x++) {
                _drawCell(ctx, {x, y});
            }
        }
    }

    function _drawCell(ctx, point) {
        const cellOrigin = _getCellOrigin(point);
        const cellValue = _getCellValue(point);

        ctx.lineWidth = CELL_BORDER_WIDTH;

        if(_isOnShortestPath(point)) {
            ctx.fillStyle = '#FF8C00';
        } else if(_isStart(point)) {
            ctx.fillStyle = '#008E00';
        } else if(_isEnd(point)) {
            ctx.fillStyle = '#0000FF';
        } else if(cellValue === 'X') {
            ctx.fillStyle = '#303030';
        } else if(cellValue === '.') {
            ctx.fillStyle = _hasDistance(point) ? '#60A462' : '#C0C0C0';
        } else {
            ctx.fillStyle = _hasDistance(point) ? '#99FF9C' : '#FFFFFF';
        }

        ctx.fillRect(cellOrigin.x, cellOrigin.y, CELL_LENGTH, CELL_LENGTH);
        ctx.strokeRect(cellOrigin.x, cellOrigin.y, CELL_LENGTH, CELL_LENGTH);
    }

    function _getCellOrigin(point) {
        return {x: point.x * CELL_LENGTH, y: point.y * CELL_LENGTH};
    }

    function _getCellValue(point) {
        return GRID[point.y][point.x];
    }

    function _isStart(point) {
        return _isPoint(start, point);
    }

    function _isEnd(point) {
        return _isPoint(end, point);
    }

    function _isPoint(point, other) {
        return point.x === other.x && point.y === other.y;
    }

    function _getDistance(point) {
        return _getIn2dArray(distance, point);
    }

    function _setDistance(point, value) {
        _setIn2dArray(distance, point, value);
    }

    function _hasDistance(point) {
        return _getDistance(point) < Infinity;
    }

    function _getPrevious(point) {
        return _getIn2dArray(previous, point);
    }

    function _setPrevious(point, value) {
        _setIn2dArray(previous, point, value);
    }

    function _getIn2dArray(array2d, point) {
        return array2d[point.y][point.x];
    }

    function _setIn2dArray(array2d, point, value) {
        array2d[point.y][point.x] = value;
    }

    function _reset() {
        _setTimeToCalculateVisible(false);
        _resetArrays();
    }

    function _resetArrays() {
        shortestPath.clear();

        for(let y = 0; y < GRID_MAX_Y; y++) {
            distance[y] = [];
            previous[y] = [];

            for(let x = 0; x < GRID_MAX_X; x++) {
                _setDistance({x, y}, Infinity);
                _setPrevious({x, y}, null);
            }
        }
    }

    async function _calculateShortestPath() {
        const startDate = new Date();

        _setControlsEnabled(false);
        _reset();
        _setDistance(start, 0);

        const availablePoints = _getAllPoints();
        let minDistancePoint = _getMinDistancePoint(availablePoints);
        let ticks = 0;

        while(minDistancePoint && availablePoints.size) {
            availablePoints.delete(_pointToNum(minDistancePoint));
            const neighborPoints = _getNeighboringPoints(minDistancePoint, availablePoints);

            neighborPoints.forEach((neighborPoint) => {
                const alt = _getDistance(minDistancePoint) + _getSlowDownAtPoint(neighborPoint);

                if(alt < _getDistance(neighborPoint)) {
                    _setDistance(neighborPoint, alt);
                    _setPrevious(neighborPoint, minDistancePoint);
                }
            });

            minDistancePoint = _getMinDistancePoint(availablePoints);

            if((++ticks % MAX_TICKS_BEFORE_PAINT) === 0) {
                await sleep(0);
            }
        }

        shortestPath = _getShortestPath();
        _setTimeToCalculate(startDate, new Date());
        _setTimeToCalculateVisible(true);
        _setControlsEnabled(true);
    }

    function _getAllPoints() {
        const all = new Set();

        for(let y = 0; y < GRID_MAX_Y; y++) {
            for(let x = 0; x < GRID_MAX_X; x++) {
                all.add(_pointToNum({x, y}));
            }
        }

        return all;
    }

    function _pointToNum(point) {
        return GRID[0].length * point.y + point.x;
    }

    function _numToPoint(s) {
        const y = Math.floor(s / GRID_MAX_X);
        const x = s - y * GRID_MAX_X;

        return {x, y};
    }

    function _getMinDistancePoint(availablePoints) {
        let minDistance = Infinity;
        let minPoint = null;

        availablePoints.forEach((availablePoint) =>  {
            const point = _numToPoint(availablePoint);
            const d = _getDistance(point);

            if(d < minDistance) {
                minDistance = d;
                minPoint = point;
            }
        });

        return minPoint;
    }

    function _getNeighboringPoints(point, availablePoints) {
        const neighbors = [];

        let neighbor = {x: point.x, y: point.y - 1}; // up
        if(_canMoveToPoint(neighbor, availablePoints)) {
            neighbors.push(neighbor);
        }

        neighbor = {x: point.x, y: point.y + 1}; // down
        if(_canMoveToPoint(neighbor, availablePoints)) {
            neighbors.push(neighbor);
        }

        neighbor = {x: point.x - 1, y: point.y}; // left
        if(_canMoveToPoint(neighbor, availablePoints)) {
            neighbors.push(neighbor);
        }

        neighbor = {x: point.x + 1, y: point.y}; // right
        if(_canMoveToPoint(neighbor, availablePoints)) {
            neighbors.push(neighbor);
        }

        return neighbors;
    }

    function _canMoveToPoint(point, availablePoints) {
        return point.y > -1 &&
            point.y < GRID_MAX_Y &&
            point.x > -1 &&
            point.x < GRID_MAX_X &&
            _getCellValue(point) !== 'X' &&
            availablePoints.has(_pointToNum(point));
    }

    function _getSlowDownAtPoint(point) {
        const cellVal = _getCellValue(point);
        let slowDown = null;

        if(cellVal === 'X') {
            slowDown = Infinity
        } else if(cellVal === '.') {
            slowDown = 2
        } else {
            slowDown = 1
        }

        return slowDown;
    }

    function _getShortestPath() {
        const spath = new Set();
        let prev = end;

        while(prev) {
            spath.add(_pointToNum(prev));
            prev = _getPrevious(prev);
        }

        return spath;
    }

    function _isOnShortestPath(point) {
        return shortestPath.has(_pointToNum(point))
    }

    async function sleep(time) {
        return new Promise((resolve)  => {
            setTimeout(() => resolve(), time);
        });
    }

    function _setTimeToCalculate(startDate, endDate) {
        document.querySelector('.time-to-calculate').innerHTML =
            `Calculated in only ${(endDate.getTime() - startDate.getTime()) / 1000} seconds!`
    }

    function _setTimeToCalculateVisible(visible) {
        document.querySelector('.time-to-calculate').style.display = visible ? 'block' : 'none';
    }

    function _setControlsEnabled(enabled) {
        controlsEnabled = enabled;
        ['calculate-button', 'reset-button']
            .forEach((id) => document.querySelector(`#${id}`).disabled = !enabled);
    }

    function _mapClicked(event) {
        event.stopPropagation();
        event.preventDefault();

        if(controlsEnabled) {
            const point = _getClickPoint(event);

            if (event.button === 0) {
                start = point;
            } else {
                end = point;
            }

            _updateStartEndDisplay();
        }
    }

    function _getClickPoint(event) {
        const rect = event.target.getBoundingClientRect();
        const xPx = event.clientX - rect.left;
        const yPx = event.clientY - rect.top;

        return {
            x: Math.floor(xPx / CELL_LENGTH),
            y: Math.floor(yPx / CELL_LENGTH)
        };
    }

    function _updateStartEndDisplay() {
        document.querySelector('.start-x').innerHTML = start.x;
        document.querySelector('.start-y').innerHTML = start.y;
        document.querySelector('.end-x').innerHTML = end.x;
        document.querySelector('.end-y').innerHTML = end.y;
    }
})();
